﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTests;
using System.Data.Entity;
using Ploeh.AutoFixture;

namespace UnitTest.Tests
{
    [TestClass]
    public class MathUtilsTests
    {
        [TestMethod]
        public void TestAdd_WhenValidParameters_ReturnsCorrectResult()
        {
            //Arrange
            var mathUtilsUnderTest = new MathUtils();

            //Act
            var result = mathUtilsUnderTest.Add(4, 6);

            //Assert
            Assert.AreEqual(10, result);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void TestDivide_WhenDivideByZero_ThrowsException()
        {
            //Arrange
            var mathUtilsUnderTest = new MathUtils();

            //Act
            var result = mathUtilsUnderTest.Divide(4, 0);
        }
    }
}
