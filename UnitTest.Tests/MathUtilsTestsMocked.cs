﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTests;
using Rhino.Mocks;
using Ploeh.AutoFixture;

namespace UnitTest.Tests
{
    [TestClass]
    public class MathUtilsTestsMocked
    {
        [TestMethod]
        public void TestAdd_WhenValidParameters_ReturnsCorrectResult()
        {
            //Arrange
            var databaseMock = MockRepository.GenerateMock<IDatabase>();

            var mathUtilsUnderTest = new MathUtils(databaseMock);

            //Act
            var result = mathUtilsUnderTest.Add(4, 6);

            //Assert
            Assert.AreEqual(10, result);
        }

        [TestMethod]
        public void TestAdd_WhenValidParameters_ReturnsCorrectResult_WithFixture()
        {
            //Arrange
            var databaseMock = MockRepository.GenerateMock<IDatabase>();

            var fixture = new Fixture();
            var mathUtilsUnderTest = new MathUtils(databaseMock);

            var firstValue = fixture.Create<int>();
            var secondValue = fixture.Create<int>();

            //Act
            var result = mathUtilsUnderTest.Add(firstValue, secondValue);

            //Assert
            Assert.AreEqual(firstValue + secondValue, result);
        }

        [TestMethod]
        public void TestSquareSum_WhenSumExistsInDb_ReturnsSquaredSum()
        {
            //Arrange
            var databaseMock = MockRepository.GenerateMock<IDatabase>();
            var sumOperationToReturn = new SumOperation{
                Result = 10
            };

            databaseMock
                .Stub(p => p.GetSumOperation(Arg<int>.Is.Anything, Arg<int>.Is.Anything))
                .Return(sumOperationToReturn);

            var mathUtilsUnderTest = new MathUtils(databaseMock);

            //Act
            var result = mathUtilsUnderTest.SquareSum(4, 6);

            //Assert
            databaseMock.AssertWasCalled(p => p.GetSumOperation(4, 6));
            Assert.AreEqual(100, result);
        }
    }
}
