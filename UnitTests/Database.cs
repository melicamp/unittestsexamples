﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    public class MyDatabase : IDatabase
    {
        MyDbContext _dbContext;

        public MyDatabase()
        {
            _dbContext = new MyDbContext();
        }

        public void Insert(int firstValue, int secondValue, int result)
        {
            var sumOperation = new SumOperation
            {
                FirstValue = firstValue,
                SecondValue = secondValue,
                Result = result
            };

            _dbContext.SumOperations.Add(sumOperation);
            _dbContext.SaveChanges();
        }


        public SumOperation GetSumOperation(int firstValue, int secondValue)
        {
            return _dbContext.SumOperations
                .SingleOrDefault(p => p.FirstValue == firstValue && p.SecondValue == secondValue);
        }
    }
}
