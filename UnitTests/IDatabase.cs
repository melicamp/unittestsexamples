﻿using System;
namespace UnitTests
{
    public interface IDatabase
    {
        void Insert(int firstValue, int secondValue, int result);
        SumOperation GetSumOperation(int firstValue, int secondValue);
    }
}
