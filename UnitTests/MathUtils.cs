﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    public class MathUtils
    {
        private readonly IDatabase _database;

        public MathUtils()
        {
            //Strong dependency to database
            _database = new MyDatabase();
        }

        public MathUtils(IDatabase database)
        {
            _database = database;
        }

        public int Add(int first, int second)
        {
            var result =  first + second;

            _database.Insert(first, second, result);

            return result;
        }

        public int SquareSum(int first, int second)
        {
            var sumOperation = _database.GetSumOperation(first, second);

            if (sumOperation != null)
            {
                var sumOperationResult = sumOperation.Result;
                var result = sumOperationResult * sumOperationResult;
                return result;
            }

            throw new KeyNotFoundException();
        }

        public double Divide(int first, int second)
        {
            return first / second;
        }
    }
}
