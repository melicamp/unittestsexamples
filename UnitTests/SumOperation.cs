﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    public class SumOperation
    {
        public int Id { get; set; }
        [Index("IX_FirstAndSecond", 1, IsUnique = true)]
        public int FirstValue { get; set; }
        [Index("IX_FirstAndSecond", 2, IsUnique = true)]
        public int SecondValue { get; set; }
        public int Result { get; set; }
    }
}
