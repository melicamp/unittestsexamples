namespace UnitTests.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SumOperations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstValue = c.Int(nullable: false),
                        SecondValue = c.Int(nullable: false),
                        Result = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.FirstValue, t.SecondValue }, unique: true, name: "IX_FirstAndSecond");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.SumOperations", "IX_FirstAndSecond");
            DropTable("dbo.SumOperations");
        }
    }
}
