﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    class Program
    {
        static void Main(string[] args)
        {
            var mathUtils = new MathUtils();

            Console.WriteLine("Input first number.");
            var firstNumber = int.Parse(Console.ReadLine());

            Console.WriteLine("Input second number.");
            var secondNumber = int.Parse(Console.ReadLine());

            Console.WriteLine("Wait for it...");
            var sum = mathUtils.Add(firstNumber, secondNumber);
            
            Console.WriteLine(string.Format("Sum: {0}", sum));
            Console.ReadKey();
        }
    }
}
